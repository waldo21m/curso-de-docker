@extends('layout')

@section('title', "Usuario {$user->id}")

@section('content')
    <h2>{{ $title }} #{{ $user->id }}</h2>
    <br>
    <table class="table table-hover">
        <tr>
            <th>Nombre del usuario:</th>
            <td>{{ $user->name }}</td>
        </tr>
        <tr>
            <th>Correo electrónico:</th>
            <td>{{ $user->email }}</td>
        </tr>
    </table>
    <p>
        {{--<a href="{{ url("/usuarios") }}">Regresar al listado de usuarios</a>--}}
        {{--Otro méyodo en vez de estarle pasando una url es la siguiente--}}

        {{--Método que devuelve una url anterior--}}
        {{--<a href="{{ url()->previous() }}">Regresar al listado de usuarios</a> --}}

        {{--Método para ejecutar una acción del controlador--}}
        {{--<a href="{{ action("UserController@index") }}">Regresar al listado de usuarios</a>--}}
        {{--Aunque este último helper no es muy utilizado--}}

        {{--Método route--}}
        <a href="{{ route("users.index") }}">Regresar al listado de usuarios</a>
    </p>
@endsection

@section('sidebar')
    <p class="text-center"><img src="{{ asset('img/user.png') }}" alt="User Pic" height="200"></p>
@endsection