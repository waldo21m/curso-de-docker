Escribir helper para Docker Compose

Docker provee una manera eficaz de trabajar con contenedores, sin embargo, el
uso de su sintaxis puede no ser muy amigable para el usuario. Es por eso que en
esta lección crearemos un helper que nos permitirá ejecutar tareas de Docker
Compose con un simple comando, incrementando la productividad de los
desarrolladores y facilitando la ejecución de los contenedores.

Notas:

Para poder crear nuestro helper vamos a necesitar de un archivo nuevo, el cuál
podemos crearlo con el comando touch develop. En ese archivo es donde vamos a
poner todo el código necesario para ejecutar las tareas. En esta ocasión vamos
a estar trabajando con Bash debido a que es fácilmente reconocido por sistemas
basados en Unix como lo son Linux y Mac.

Dentro de nuestro archivo develop vamos a poner algo como lo siguiente:

#!/usr/bin/env bash

# Exportar variables para poder utilizarlas con docker-compose
export APP_PORT=${APP_PORT:-8080}
export APP_ENV=${APP_ENV:-local}
export DB_PORT=${DB_PORT:-33060}
export DB_ROOT_PASSWORD=${DB_ROOT_PASSWORD:-root}
export DB_NAME=${DB_NAME:-styde}
export DB_USER=${DB_USER:-styde}
export DB_PASSWORD=${DB_PASSWORD:-secret}

COMPOSE="docker-compose"

# Verificamos si estamos enviando argumentos
if [ $# -gt 0 ]; then
    # Ejecutar comandos de artisan
    if [ "$1" == "art" ]; then
        shift 1
        $COMPOSE run --rm \
            -w /var/www/html \
            php \
            php artisan "$@"
    # Ejecutar tareas de composer
    elif [ "$1" == "composer" ]; then
        shift 1
        $COMPOSE run --rm \
            -w /var/www/html \
            php \
            composer "$@"
    # Ejecutar pruebas de phpunit
    elif [ "$1" == "test" ]; then
        shift 1
        $COMPOSE run --rm \
            -w /var/www/html \
            php \
            ./vendor/bin/phpunit "$@"
    else
        # Si los argumentos son diferentes se pasarán a docker-compose
        $COMPOSE "$@"
    fi
else
    # Si no se indica ningún parámetro se mostrarán los contenedores
    docker-compose ps
fi

Básicamente aquí vamos a revisar los argumentos que se envian una vez ejecutemos
nuestro script. Si estos argumentos coinciden con los comandos de artisan,
composer o test entonces se ejecutarán los comandos especificados en cada una de
sus secciones, en caso contrario, si se envía un argumento pero no tiene nada que
ver con los comandos mencionados anteriormente, se tomarán como argumentos para
docker-compose.

Mis apuntes:

El nombre del archivo del helper puede ser cualquiera y dentro del archivo vamos
a colocar un poco de código bash (para entender esto se debe tomar un curso
de bash).

La primera línea de código indica el entorno en el que se va a ejecutar el 
script:
#!/usr/bin/env bash

Lo siguiente es exportar variables para poder utilizarlas con docker-compose y
si estas no son asignadas, ya tienen valores por defecto
export APP_PORT=${APP_PORT:-8080}
export APP_ENV=${APP_ENV:-local}
export DB_PORT=${DB_PORT:-33060}
export DB_ROOT_PASSWORD=${DB_ROOT_PASSWORD:-root}
export DB_NAME=${DB_NAME:-styde}
export DB_USER=${DB_USER:-styde}
export DB_PASSWORD=${DB_PASSWORD:-secret}

La siguiente es una variable que va a contener el comando docker-compose.
COMPOSE="docker-compose"

Para comprobar que todo va marchando bien usaremos el bloque descrito 
anteriormente:

#!/usr/bin/env bash

export APP_PORT=${APP_PORT:-8080}
export APP_ENV=${APP_ENV:-local}
export DB_PORT=${DB_PORT:-33060}
export DB_ROOT_PASSWORD=${DB_ROOT_PASSWORD:-root}
export DB_NAME=${DB_NAME:-styde}
export DB_USER=${DB_USER:-styde}
export DB_PASSWORD=${DB_PASSWORD:-secret}

COMPOSE="docker-compose"

docker-compose ps

Salimos del archivo y desde el git bash ejecutamos ./develop. Si funciona
correctamente debe salir el texto de docker-compose ps

Seguimos trabajando con el archvio develop. Verificamos si estamos enviando
argumentos, y para ello usamos este bloque if. Esto sirve para comprobar si
estamos enviando argumentos al script. El $# nos va a devolver todos los
argumentos que estamos enviando al script y si es mayor a 0 es que tenemos
argumentos.
if [ $# -gt 0 ]; then
    echo 'Hola Styde'
else
    docker-compose ps
fi
Los bloque if se finalizan escribiendolo de forma inversa, es decir, fi.
Para hacer una prueba sencilla usamos el siguiente código:

#!/usr/bin/env bash

export APP_PORT=${APP_PORT:-8080}
export APP_ENV=${APP_ENV:-local}
export DB_PORT=${DB_PORT:-33060}
export DB_ROOT_PASSWORD=${DB_ROOT_PASSWORD:-root}
export DB_NAME=${DB_NAME:-styde}
export DB_USER=${DB_USER:-styde}
export DB_PASSWORD=${DB_PASSWORD:-secret}

COMPOSE="docker-compose"

if [ $# -gt 0 ]; then
    echo 'Hola Styde'
else
    docker-compose ps
fi

Y lo corremos con estos comandos
./develop
./develop 1

Si todo marcha bien, debe salir el texto del docker-compose ps y el Hola Styde
en el segundo comando. Sigamos agregando más funcionalidad en el if de la
siguiente manera:

Si el primer argumento es art, vamos a ejecutar comando de artisan en un nuevo
contenedor. Verificamos que nuestro primer argumento (con $1) es art y si es así
hacemos un shift 1 (Ir al final esta explicación) y ejecutamos $COMPOSE que se lee como
docker-compose... Generará un contenedor que automáticamente se va a eliminar
una vez que termine su función, va a establecer el directorio activo como 
/var/www/html que es donde está el proyecto. El php indica que vamos a usar EL
SERVICIO de php (OJO: No es el nombre del contenedor, es el nombre del servicio
indicado en el archivo docker-compose.yml). Luego ejecutamos php artisan y el
"$@" es una variable especial de bash que contiene todos los argumentos que
estamos pasando al script.
Pero existe un problema... Si $@ contiene todos los argumentos, esto se 
interpreta al ejecutar ./develop art migrate como "php artisan art migrate",
cosa que no está bien. Aquí viene a salvarnos el comando shift 'n' que lo que
hace es saltar n argumentos, en este caso 1. Entonces va a saltar art y se
leería como "php artisan migrate".
    if [ "$1" == "art" ]; then
        shift 1
        $COMPOSE run --rm \
            -w /var/www/html \
            php \
            php artisan "$@"

Haremos lo mismo con composer con un elif (else if).
    elif [ "$1" == "composer" ]; then
        shift 1
        $COMPOSE run --rm \
            -w /var/www/html \
            php \
            composer "$@"

Pruebas de Laravel
    elif [ "$1" == "test" ]; then
        shift 1
        $COMPOSE run --rm \
            -w /var/www/html \
            php \
            ./vendor/bin/phpunit "$@"
    else

Si los argumentos son diferentes se pasarán a docker-compose
        $COMPOSE "$@"
    fi

Finalmente quedaría como:

#!/usr/bin/env bash

# Exportar variables para poder utilizarlas con docker-compose
export APP_PORT=${APP_PORT:-8080}
export APP_ENV=${APP_ENV:-local}
export DB_PORT=${DB_PORT:-33060}
export DB_ROOT_PASSWORD=${DB_ROOT_PASSWORD:-root}
export DB_NAME=${DB_NAME:-styde}
export DB_USER=${DB_USER:-styde}
export DB_PASSWORD=${DB_PASSWORD:-secret}

COMPOSE="docker-compose"

# Verificamos si estamos enviando argumentos
if [ $# -gt 0 ]; then
    # Ejecutar comandos de artisan
    if [ "$1" == "art" ]; then
        shift 1
        $COMPOSE run --rm \
            -w /var/www/html \
            php \
            php artisan "$@"
    # Ejecutar tareas de composer
    elif [ "$1" == "composer" ]; then
        shift 1
        $COMPOSE run --rm \
            -w /var/www/html \
            php \
            composer "$@"
    # Ejecutar pruebas de phpunit
    elif [ "$1" == "test" ]; then
        shift 1
        $COMPOSE run --rm \
            -w /var/www/html \
            php \
            ./vendor/bin/phpunit "$@"
    else
        # Si los argumentos son diferentes se pasarán a docker-compose
        $COMPOSE "$@"
    fi
else
    # Si no se indica ningún parámetro se mostrarán los contenedores
    docker-compose ps
fi

Pero gracias a este archivo, ya no necesitaremos nuestro .env porque las 
variables del docker-compose.yml se están indicando en el archivo develop.
Comprobemos que esto está funcionando ejecutando:
./develop art -V
Esto debe retornar la versión de Laravel que estamos utilizando.

Para ver la versión de Composer lo hacemos con el comando:
./develop composer -V

Para ejecutar las pruebas de phpunit lo ejecuto con el comando:
./develop test -V

Y si simplemente escribimos:
./develop ps

Lo que hará el bash es que como no entra en ninguna de las condiciones, 
ejecutará:
docker-compose ps

Pero es equivalente a escribir ./develop simplemente ya que está en el primer
condicional del archivo. De esta manera escribimos un helper que nos 
simplificará las labores diarias.

P.D Este bash ./develop no funciona en Windows, es solo para Linux y Mac. Es
por eso que tampoco elimino el archivo .env. Para solventar esto debemos crear
a pie un contenedor para ejecutar los comandos como en la lección #23.